package gui;

import java.util.Scanner;

/**
 * @file Calculator.java
 * @date 24.11.2016
 * @author Mathieu Monteverde, Mika Pagani
 * @brief Class Calculator. Cette classe est l'implémentation de la calculatrice
 *        dans la console au lieu de l'interface graphique fournie.
 */
public class Calculator {
   
   // L'état actuel de la calculette.
   private final State s = new State();
   
   /**
    * @brief Affiche dans la console le contenu de la pile de l'état actuel
    */
   private void displayStack() {
      Object[] o = s.getStackArray();
      for (Object o1 : o) {
         System.out.print(o1+" ");
      }
      System.out.println("");
   }
   
   /**
    * @brief Apllique une operation à la pile et adapte le comportement du programme
    *        car il n'est pas le même que dans l'interface graphique.
    * @param o l'operation à appliquer
    */
   private void execOperator(Operator o) {
      s.setValue(Double.toString(s.pilePop()));
      o.execute();
      s.pushCurrentValue();
   }
   
   /**
    * @brief Execute le programme de la calculatrice mais dans la console au lieu du gui
    */
   private void execute() {
      Scanner scanf = new Scanner(System.in);
      
      boolean endProgram = false;
      String input;
      Operator o;
      
      while(!endProgram) {
         
         System.out.print("> ");
         input = scanf.next();
         
         //Traitement de l'input
         switch (input) {
            
            case "exit": endProgram = true;
               break;
            
            case "+":
               execOperator(new Addition(s));
               break;
            case "-":
               execOperator(new Substraction(s));
               break;
            case "*":
               execOperator(new Multiplication(s));
               break;
            case "/":
               execOperator(new Division(s));
               break;
            case "sqrt":
               execOperator(new SquareRoot(s));
               break;
            case "x^2":
               execOperator(new SquarePower(s));
               break;
            case "1/x":
               execOperator(new Inverse(s));
               break;
            case "MS":
               execOperator(new MemoryStore(s));
               break;
            case "MR":
               o = new MemoryRecall(s);
               break;
            case "CE":
               execOperator(new ClearError(s));
               break;
            case "C":
               execOperator(new Clear(s));
               break;
            default:
               s.setValue(input);
               s.pushCurrentValue();
               break;
         }
         if(s.isStateError()) {
            System.out.print("Error :"+s.getErrorMessage());
            System.out.println("");
         } else {
            displayStack();
         }
      }
   }
   
   /**
    * @brief Main permettant de lancer la calculatrice dans la console.
    * @param args Arguments donnés lors de l'execution du programme.
    */
   public static void main(String args[]) {
      Calculator c = new Calculator();
      c.execute();
  }
}
