package gui;
/**
 * @file Operator.java
 * @date 24.11.2016
 * @author Mika Pagani, Mathieu Monteverde
 * 
 * Hiérarchie de classes issues de la classe Operator. Ces classes implémentent des 
 * opérations sur un état de type State. 
 * 
 */

/**
 * @brief Class abstraite Operator. Cette classe garde en mémoire un objet de type 
 * State sur lequel s'appliqueront les opérateurs qui en héritent.
 * 
 * - Les opérateurs de cette classe se chargent de maintenir le comportement de
 *   l'interface utilisateur consistant à remplacer une valeur vide par un 0. 
 * - Ils se chargent aussi de maintenir le comportement qu'un résultat de calcul ne 
 *   peut pas être modifié.
 */
abstract public class Operator {
   
   private final State s; // L'état sur lequel travaillent les opérateurs
   
   // Constructeur
   public Operator(State s) {
      this.s = s;
   }
   
   // Méthode appelée qui effectue l'opération définie dans l'opérateur
   abstract public void execute();
   
   /**
    * Retourne l'état sur lequel travaillent les opérateurs enfants
    * @return Retourne l'état courant
    */
   protected State getState() {
      return s;
   }
}

/**
   La classes InputValue regroupe les opérateurs qui entrent ou modifient la valeur 
 * courante en la complétant ou en supprimant une partie. Les classes en héritant 
 * traitent donc la valeur courante comme une String modifiable.
*/
abstract class InputValue extends Operator {
   
   /**
    * @param s l'état State sur lequel travail l'opérateur
    */
   public InputValue(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      /*
         Si la valeur courante est le résultat d'une opération, on la considère
         comme non-modifiable et on la pousse d'abord sur la pile.
      */
      if ( getState().isResult() ) {
         getState().pushCurrentValue();
      }
      
      /*
         Remplacer la valeur courante par la valeur modifiée par l'opérateur.
      */
      getState().setValue( resultat( getState().getCurrentValue() ) );
   }
   
   /**
    * Reçoit une valeur sous forme de String et retourne une valeur modifiée. 
    * 
    * @param v la valeur à modifier par l'opérateur
    * @return la valeur modifie
    */
   abstract public String resultat( String v );
}

/**
   Opérateur qui ajoute un digit à la valeur actuelle dans la calculette. 
*/
class AddDigit extends InputValue {
   
   private String value = "";
   
   /**
    * @param s l'état State sur lequel travaille l'opérateur
    * @param i le digit associé à l'opérateur
    */
   public AddDigit(State s, int i) {
      super(s);
      this.value = Integer.toString(i);
   }
   
   @Override
   public String resultat( String v) {
      return v + value;
   }
}
 /**
  * @brief Supprime un digit de la valeur courante.
  */
class RemoveDigit extends InputValue {
   
   /**
    * @param s l'état State sur lequel travaille l'opérateur
    */
   public RemoveDigit(State s) {
      super(s);
   }
   
   @Override
   public String resultat(String v) {
      // Si la valeur n'est pas une String vide, on enlève le dernier caractère
      if (v.length() > 0) {
         v = v.substring(0, v.length() - 1);
      }
      return v;
   }
}

/**
 * Opérateur qui ajoute un point décimal à la valeur courante. Dans cette 
 * implémentation, on ne permet pas d'ajouter plus d'un point à une valeur, 
 * simplement par souci de cohérence.
 */
class AddDot extends InputValue {
   public AddDot(State s) {
      super(s);
   }
   
   @Override 
   public String resultat(String v) {
      /*
         On verifie qu'il n'y a pas déjà un point décimal dans la valeur.
      */
      if(v.indexOf('.') == -1) {
         if (v.length() == 0)
            v = "0";
         return v + ".";
      } else {
         return v;
      }
   }
}


/**
 * Opérateur qui change le signe de la valeur courante.
 */
class Sign extends InputValue {
   public Sign(State s) {
      super(s);
   }
   
   @Override
   public String resultat(String v) {
      
      // Si la valeur est positive
      if(v.indexOf('-') == -1) {
         return "-" + v;
      }
      
      // Si la vaeur est négative et que la valeur n'est pas une String vide
      if (v.length() > 1)
         return v.substring(1);
      
      return "0";
      
   }
}


/**
 * Opérateur qui sauvegarde la valeur courante en mémoire.
/**
 * @brief Sauvegarde la valeur courante en mémoire.
 */
class MemoryStore extends Operator {
   public MemoryStore(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      getState().storeMemory();
   }
}

/**
 * @brief Récupère la valeur en mémoire.
 */
class MemoryRecall extends Operator {
   public MemoryRecall(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      getState().recallMemory();    
   }
}

/**
 * Opérateur qui reset l'état d'erreur du State. Cela revient à réinitialiser 
 * la valeur courante et le mode erreur.
 */
class ClearError extends Operator {
   public ClearError(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      getState().clearError();
   }
}

/**
 * Opérateur qui reset l'état du State. Cela revient à reset l'état d'erreur du State
 * et à vider la pile.
 */
class Clear extends ClearError {
   public Clear(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      getState().clear();
   }
}

/**
 * @brief Permet de mettre la valeur courante dans la pile
 */
class Enter extends Operator {
   public Enter(State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      if ( getState().getCurrentValue().length() == 0) {
         getState().setValue("0");
      }
      getState().pushCurrentValue();
   }
}

/**
 * Classe qui définit des opérateurs travaillant avec des opérations arithmétiques.
 * Les opérateurs qui en héritent travaillent avec des valeurs de type double. Ils ne 
 * modifient pas les valeurs et les utilisent comme opérandes. Il modifient ensuite
 * la valeur courante dans le State sous forme de résultat. 
 * 
 * Ces opérateurs utilisent la valeur courante comme un double dans leur calcul. Ils 
 * demandent donc d'abord au State de vérifier que celle-ci est utilisable. 
 */
abstract class ArithmeticOperator extends Operator {
   public ArithmeticOperator (State s) {
      super(s);
   }
   
   @Override
   public void execute() {
      // Si la valeur courante est une valeur utilisable comme double
      if ( getState().isCurrentValueValid() ) {
         String v = getState().getCurrentValue();
         
         if (v.length() == 0)
            v = "0";

         double op1 = Double.parseDouble(v);
         
         // Stocker le résultat du calcul sous forme de résultat
         getState().setResult( resultat(op1) );
      }
   }
   
   /**
    * @param op1 le premier opérande de l'opérateur
    * @return le résultat de l'opération
    */
   abstract public double resultat(double op1);
}

/**
 * Représentation abstraite des Opérateurs à deux opérandes. Ils utilisent le 
 * premier opérande récupéré dans ArithmeticOperator et le second qu'ils récupèrent 
 * sur la pile. 
 */
abstract class BinaryOperator extends ArithmeticOperator {
   public BinaryOperator(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double op1) {
      double op2 = getState().pilePop();
      return resultat(op2, op1);
   }
   
   /**
    * Surcharge de la méthode résultat pour les opérateurs à deux opérandes
    * @param op1 le premier opérande, récupéré sur la pile
    * @param op2 le second opérande récupéré dans la valeur courante
    * @return le résultat de l'opération
    */
   abstract double resultat(double op1, double op2);
}

/**
 * @brief Class définissant l'addition.
 */
class Addition extends BinaryOperator {
   public Addition(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a, double b) {
      return a + b;
   }
}

/**
 * @brief Class définissant la soustraction.
 */
class Substraction extends BinaryOperator {
   public Substraction(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a, double b) {
      return a - b;
   }
}

/**
 * @brief Class définissant la multiplication.
 */
class Multiplication extends BinaryOperator {
   public Multiplication(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a, double b) {
      return a * b;
   }
}

/**
 * @brief Class définissant la division.
 */
class Division extends BinaryOperator {
   public Division(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a, double b) {
      return a / b;
   }
}

/**
 * Représentation abstraite des Opérateurs qui réalisent des opérations à un opérande.
 */
abstract class UnaryOperator extends ArithmeticOperator {
   public UnaryOperator(State s) {
      super(s);
   }
   
   @Override
   abstract public double resultat(double a);
}

/**
 * Classe qui calcule l'inverse d'une valeur.
 */
class Inverse extends UnaryOperator {
   public Inverse(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a) {
      return 1 / a;
   }
}

/**
 * Classe qui calcule le carré d'un nombre.
 */
class SquarePower extends UnaryOperator {
   public SquarePower(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a) {
      return a * a;
   }
}

/**
 * Classe qui calcule la racine carrée d'un nombre.
 */
class SquareRoot extends UnaryOperator {
   public SquareRoot(State s) {
      super(s);
   }
   
   @Override
   public double resultat(double a) {
      return Math.sqrt(a);
   }
}



