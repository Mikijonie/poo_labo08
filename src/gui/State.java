package gui;

import util.Stack;

/**
 * @file State.java
 * @date 24.11.2016
 * @author Mathieu Monteverde, Mika Pagani
 * @brief Implémente l'état d'une calculette fonctionnant selon la notaion polonaise 
 *        inverse. Les valeurs sont stockées sous forme de string pour une 
 *        manipulation et une verification simplifiée
 * 
 * L'état garantit l'intégrité des données de la façon suivante
 *    -  La valeur courante est stockée sous forme de String. Cette String n'est pas 
 *       garantie d'être convertible en double valide en tout temps. La fonction 
 *       isCurrentValueValide permet de tester la validité de la valeur avant de la 
 *       récupérér.
 *    -  Les valeurs enregistrées dans la pile de l'état sont des doubles.
 *    -  Aucune action n'est effectuable si l'état est en mode erreur, mis à part des
 *       fonction de réinitialisation
 */
public class State {
   
   private boolean stateError = false;  // Si l'état est en mode erreur ou non
   private boolean isResult = false;    // Si la valeur courante est un résultat ou non
   private final Stack p = new Stack(); // La pile de l'état
   private String currentValue = "";    // La valeur courante de la calculette
   private String memory = "";          // Valeur gardée en mémoire
   private String errorMessage = "";    // Message d'erreur de l'état
   
   public State() { }

   /**
    * @return Retourne la valeur au sommet de la pile. Si la pile est vide, l'état
    *         passe en mode erreur et retourne 0.
    */
   public double pilePop() {
      if ( isStateError() )
         return 0;
      
      Object o = p.pop();
      if(o == null) {
         setError("Pas assez d'opérandes dans la pile");
         return 0;
      } else {
         return (double)o;
      }
   }
   
   /**
    * @brief Vide la pile
    */
   private void clearPile() {
      if ( !isStateError() )
         p.clear();
   }

   /**
    * @return  la valeur courante de l'état
    */
   public String getCurrentValue() {
      return currentValue;
   }
   
   /**
    * @brief Placer un résultat dans la valeur courante. Un résultat ne peut plus 
    *        être modifié mais seulement placé dans la pile.
    * @param v double à définir comme resultat
    */
   public void setResult(double v) {
      if ( !isStateError() ) {
         currentValue = Double.toString(v);
         isResult = true;
         checkIntegrity();
      }
   }
   
   /**
    * @brief Définir la valeur courante. 
    * @param v la nouvelle valeur courante
    */
   public void setValue(String v) {
      
      if ( !isStateError() ) {
         currentValue = v;
         isResult = false;
      }
   }
   
   
   /**
    * @brief Ajoute la valeur courante sur la pile. Ne fais rien si la valeur 
    *        courante est vide. 
    */
   public void pushCurrentValue() {
      checkIntegrity();
      
      if ( isStateError() )
         return;
      
      if (currentValue.length() > 0) {
         p.push( Double.parseDouble(currentValue) );
         currentValue = "";
      }
   }

   /**
    * @brief Sauvegarde la valeur courante en mémoire.
    */
   public void recallMemory() {
      if ( !isStateError() )
         currentValue = memory;
   }

   /**
    * @brief Restaure la valeur en mémoire comme valeur courante
    */
   public void storeMemory() {
      checkIntegrity();
      
      if ( isStateError() )
         return;
      
      memory = currentValue;
   }
   
   /**
    * @return L'état de la pile sous la forme d'un tableau d'objets
    */
   public Object[] getStackArray() {
      return p.getArray();
   } 
   
   /**
    * @return true si l'état est en mode erreur
    */
   public boolean isStateError() {
      return stateError;
   }
   
   /**
    * @brief Fait passe l'état en mode erreur et ajoute l'information correspondante
    *        dans le message d'erreur.
    */
   private void setError(String message) {
      stateError = true;
      errorMessage = message;
   }
   
   /**
    * @brief Réinitialise l'état, vide la valeur courante et le message d'erreur.
    */
   public void clearError() {
      currentValue = "";
      stateError = false;
      errorMessage = "";
      isResult = false;
   }
   
   /**
    * @brief Appelle clearError() et vide la pile.
    */
   public void clear() {
      clearError();
      clearPile();
   }
   
   /**
    * @return le dernier message créé.
    */
   public String getErrorMessage() {
         return errorMessage;
   }
   
   /**
    * @brief Vérifie l'intégrité des valeurs dans l'état. Les valeurs ne peuvent pas 
    *        être Infinity, NaN. Elles doivent être convertible en double.
    */
   private void checkIntegrity() {
      
      if ( isStateError() )
         return;
      
      if (currentValue.length() > 0) {
         try {
               double a = Double.parseDouble(currentValue);

               if( Double.isInfinite(a) ) {
                  setError("Valeur : ' " +currentValue+"' infinie");
               } else if ( Double.isNaN(a) ) {
                  setError("Valeur : ' " +currentValue+"' NaN");
               } 
         }
         catch (NumberFormatException e) {
            setError("Valeur : ' " +currentValue+"' n'a pas le format d'un nombre");
         }
      }
   }
   
   /**
    * @return true si la valeur courante est un résultat
    */
   public boolean isResult() {
      return isResult;
   }
   
   /**
    * Méthode à appeler avant de réaliser un calcul avec la valeur courante, étant
    * donné que celle-ci peut-être en cours de construction. 
    * Le State passe en mode error si la valeur n'est pas valide.
    * 
    * @return true si la valeur courante peut-être utilisée dans un calcul.
    */
   public boolean isCurrentValueValid() {
      checkIntegrity();
      return !isStateError();
   }
}
