package util;

/**
 * @file Iterateur.java
 * @date 24.11.2016
 * @author Mathieu Monteverde
 * @author Mika Pagani
 * 
 * But : La classe Iterateur implément un itérateur sur une suite d'éléments de type 
 * PileElement, typiquement une Pile.
 */
public class Iterateur {
   /*
      Référence vers un élément de la pile. Il est initialisé avec un élément vide 
      se situant virtuellement en position 0 de la pile (avant le head).
   */
   private StackElement element = new StackElement(null);
   
   /**
    * Constructeur d'un itérateur sur une pile. On lui passe le premier élément
    * de Pile sur lequel on veut itérer.
    *
    * @param element le premier élément de l'itération
    */
   Iterateur(StackElement element) {
      this.element = element; 
   }
   
   /**
    * Vérifie si l'élément courant possède un prochain élément.
    *
    * @return vrai si il y a un prochain élément, faux sinon
    */
   public boolean possedeSuivant() {
      return element != null;
   }
   
   /**
    * Retourne le prochain élément dans la suite d'éléments.
    *
    * @return le prochain élément
    */
   public Object suivant() {
      if (element != null) {
         StackElement temp = element;
         element = element.next();
         return temp.data();
      } else {
         return null;
      }
   }
    
}
