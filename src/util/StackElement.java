package util;

/**
 * @file StackElement.java
 * @date 24.11.2016
 * @author Mathieu Monteverde
 * @author Mika Pagani
 * 
 * But: La classe PileElement représente un élément de Pile d'objets de type Object.
 */
class StackElement {

   private final Object elementData;
   private StackElement nextElement = null;
   
   /**
    * Constructeur d'un élément d'une Pile d'Objects
    * 
    * @param data L'objet voulant être stocké
    */
   StackElement(Object data) {
      elementData = data;
   }
   
   /**
    * Récupère le prochain élément dans la pile.
    * 
    * @return le prochain élément de type PileElement
    */
   StackElement next() {
      return nextElement;
   }
   
   /**
    * Définit le prochain élément de pile.
    * 
    * @param e l'élément à définir comme suivant dans la pile
    */
   void setNext(StackElement e) {
      nextElement = e;
   }
   
   /**
    * Retourne la valeur stockée dans un élément de Pile.
    * 
    * @return Le contenu de l'élément de Pile
    */
   Object data() {
      return elementData;
   }
   
   @Override
   public String toString() {
      return elementData != null ? elementData.toString() : "";
   }
}
