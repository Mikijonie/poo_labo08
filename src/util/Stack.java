package util;

/**
 * @file Stack.java
 * @date 24.11.2016
 * @author Mathieu Monteverde
 * @author Mika Pagani
 * 
 * But: La classe Stack implémente une pile d'objets de type Object.
 */
public class Stack {
   private StackElement head = null; // Tête de la pile
   
   /**
    * Constructeur d'une pile vide
    */
   public Stack() {  }
   
   /**
    * Retirer l'élément au sommet de la pile.
    *
    * @return l'élément au sommet de la pile ou null si la pile est vide.
    */
   public Object pop() {
      if ( empty() ) {
         return null;
      }
      StackElement temp = head;
      head = head.next();
      
      return temp.data();
   }
   
   /**
    * Ajouter un élément au sommet de la pile.
    *
    * @param e l'élément à ajouter au sommet de la pile
    */
   public void push(Object e) {
      StackElement temp = head;
      head = new StackElement(e);
      head.setNext(temp);
   }
   
   /**
    * Savoir si la pile est vide
    *
    * @return vrai si la pile est vide, faux sinon
    */
   public boolean empty() {
      return this.head == null;
   }
   
   /**
    * Vide la pile
    */
   public void clear() {
      head = null;
   }
   
   @Override
   public String toString() {
      String resultat = "";
      Iterateur i = getIterateur();
      while ( i.possedeSuivant() ) {
         resultat += "<" + i.suivant() + ">";
      }
      
      return resultat;
   }
   
   /**
    * Construit un tableau d'objets correspondant à l'état de la pile.
    *
    * @return un tableau d'Object identique au contenu de la pile dont l'indice 0 
    * correspond au sommet de la pile.
    */
   public Object[] getArray() {
      Object[] elements = new Object[size()];
      Iterateur i = getIterateur();
      int cnt = 0;
      while ( i.possedeSuivant() ) {
         elements[cnt++] = i.suivant();
      }
      
      return elements;
   }
   
   /**
    * Calcule la taille de la pile.
    *
    * @return le nombre d'éléments dans la pile.
    */
   private int size () {
      int size = 0;
      Iterateur i = getIterateur();
      while ( i.possedeSuivant() ) {
         ++size; 
         i.suivant();
      }
      return size;
   }
   
   
   
   /**
    * Crée un itérateur de type Iterateur sur la pile.
    * 
    * @return un itérateur sur le premier élément de la pile.
    */
   public Iterateur getIterateur() {
      return new Iterateur(head);
   }
   
   /**
    * Offre un programme de test des fonctionnalités de la classe Pile.
    */
   public static void test() {
      Stack p = new Stack();
      
      System.out.println("Itérateur sur une pile vide :");
      Iterateur it1 = p.getIterateur();
      while ( it1.possedeSuivant() ) {
         System.out.println( it1.suivant() );
      }
      
      System.out.println("Remplissage de la pile :");
      for(int i = 1; i <= 10; ++i) {
      
         if ( p.empty() ) {
            System.out.println("La pile est vide");
         }
         if (i % 2 == 0) {
            p.push(i);
            System.out.println("Ajout de " + i);
         } else {
            p.push("Element" + i);
            System.out.println("Ajout de Element" + i);
         }
         
         System.out.println("Etat de la pile :");
         System.out.println(p);
         System.out.println("Tableau d'objets :");
         System.out.print("[");
         Object[] objets = p.getArray();
         for (int k = 0; k < objets.length; ++k) {
            System.out.print(objets[k]);
            if (k < objets.length - 1)
               System.out.print(", ");
         }
         System.out.println("]\n");
      }
      System.out.println("Affichage avec un itérateur :");
      Iterateur it = p.getIterateur();
      while ( it.possedeSuivant() ) {
         System.out.println( it.suivant() );
      }
      
      System.out.println("Désempilage de la pile :");
      for (int i = 1; i <= 10; ++i) {
         Object o1 = p.pop();
         System.out.println("Objet retiré : ");
         System.out.println(o1);
         
         System.out.println("Etat de la pile :");
         System.out.println(p);
         System.out.println("Tableau d'objets :");
         System.out.print("[");
         Object[] objets = p.getArray();
         for (int k = 0; k < objets.length; ++k) {
            System.out.print(objets[k]);
            if (k < objets.length - 1)
               System.out.print(", ");
         }
         System.out.println("]\n");
      
         if ( p.empty() ) {
            System.out.println("La pile est vide");
         }
      }
   }
}
